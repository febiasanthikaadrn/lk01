public class Ticket {

    private String namaPenumpang;
    private String asal;
    private String tujuan;

    public Ticket(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public Ticket(String namaPenumpang, String asal, String tujuan) {
        this.namaPenumpang = namaPenumpang;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    public String getNamaPenumpang() {
        return namaPenumpang;
    }

    public void setNamaPenumpang(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public String getAsal() {
        return asal;
    }

    public void setAsal(String asal) {
        this.asal = asal;
    }

    public String getTujuan() {
        return tujuan;
    }

    public void setTujuan(String tujuan) {
        this.tujuan = tujuan;
    }

    //Untuk outputnya
    public String toString(){
        if (asal == null || tujuan == null){
            return "Nama : " + namaPenumpang;
        }else {
            return "Nama : " + namaPenumpang + "\n" + "Asal : " + asal + "\n" + "Tujuan : " + tujuan;
        }
    }
}
