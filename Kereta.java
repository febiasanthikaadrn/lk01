import java.util.ArrayList;
import java.util.List;

public class Kereta {

    private String jenisKereta;
    private int tiketTersedia;
    private List<Ticket> tiketKereta;

    //Komuter
    public Kereta() {
        this.jenisKereta = "Komuter";
        tiketTersedia = 1000;
        this.tiketKereta = new ArrayList<Ticket>();
    }

    //Pesan tiket kereta komuter
    public void tambahTiket(String namaPenumpang) {
        if (tiketTersedia > 0) {
            tiketTersedia--;
            Ticket ticket = new Ticket(namaPenumpang);
            tiketKereta.add(ticket);
            System.out.println("===================================================");
            System.out.println("Tiket berhasil dipesan.");
            } else {
            System.out.println("===================================================");
            System.out.println("Tiket kereta telah habis dipesan. Silahkan cari jadwal keberangkatan lainnya");
        }
    }

    //KAJJ
    public Kereta(String jenisKereta, int tiketTersedia) {
        this.jenisKereta = jenisKereta;
        this.tiketTersedia = tiketTersedia;
        this.tiketKereta = new ArrayList<Ticket>();
    }

    //Pesan tiket KAJJ
    public void tambahTiket(String namaPenumpang, String asal, String tujuan) {
        if (tiketTersedia > 0) {
            tiketTersedia--;
            Ticket ticket = new Ticket(namaPenumpang, asal, tujuan);
            tiketKereta.add(ticket);
            System.out.println("===================================================");
            System.out.println("Tiket berhasil dipesan. Sisa tiket tersedia: " + tiketTersedia);
            } else {
                System.out.println("===================================================");
                System.out.println("Tiket kereta telah habis dipesan. Silahkan cari jadwal keberangkatan lainnya");
            }
        }

    //Tampilan tiket
    public void tampilkanTiket() {
        System.out.println();
        System.out.println("Daftar penumpang kereta api " + jenisKereta);
        System.out.println("---------------------------------------------------");
        for (Ticket ticket : tiketKereta) {
            System.out.println(ticket);
            System.out.println("---------------------------------------------------");
        }
        System.out.println("\n");
    }
}